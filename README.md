# Cinemate API

Компонент для работы с [Cinemate.cc](http://cinemate.cc/)  [API](http://cinemate.cc/help/api/).

Частичная реализация. Только поиск фильмов и актеров и подробности

## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

Пакет приватный, установка потребует прописать репозиторий с bitbucket.

Необходимо добавить

```
"studiosite/cinemaccapi": "dev-master"
```

в секции ```require``` `composer.json` файла.

В секции ```repositories``` добавить

```
{
    "type": "vcs",
    "url": "https://bitbucket.org/studiosite/cinemaccapi"
}
```

## Использование

```php
use studiosite\cinemaccapi\components\ApiCinema;

$api = new ApiCinema([
  'apiKey' => 'MyKey'
]);

$movies = $api->getMoviesByTerm('Фильм');

// \studiosite\cinemaccapi\models\Movie[]
print_r($movies);

```

