<?php

namespace studiosite\cinemaccapi\parsers;

use studiosite\cinemaccapi\interfaces\Parser;

/**
 * Json парсер для ответа (application|text|....)/json
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class Json implements Parser
{
	/**
	* Парсинг
	* @param string $data
	* @return mixen
	*/
	public function parse($data)
	{
		return json_decode($data, true);
	}
}