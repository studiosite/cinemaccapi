<?php
namespace studiosite\cinemaccapi\interfaces;

/**
 * Интерфейс парсера для разныех форматов
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
interface Parser
{
    /**
     * Парсинг
     * @param string $data
     * @return mixen
     */
    public function parse($data);
}
