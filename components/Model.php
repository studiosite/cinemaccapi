<?php

namespace studiosite\cinemaccapi\components;

use ReflectionClass;

/**
 * Компонент модели абстрактный
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
abstract class Model
{
	/**
     * Возвращает список атрибутов
     * @return array list of attribute names.
     */
    public function attributes()
    {
        $class = new ReflectionClass($this);
        $names = [];
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }
        return $names;
    }

    /**
     * Установка атрибутов
     * @param array $values Значение атрибутов (name => value) 
     * @see attributes()
     */
    public function setAttributes($values)
    {
        if (is_array($values)) {
            $attributes = array_flip($this->attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
                    $this->$name = $value;
                }
            }
        }
    }

    /**
     * Возвращает значение атрибутов
     * @param array $names Массив имен атрибутов
     * @return array Значение атрибутов (name => value) 
     */
    public function getAttributes($names = null)
    {
        $values = [];
        if ($names === null) {
            $names = $this->attributes();
        }
        foreach ($names as $name) {
            $values[$name] = $this->$name;
        }
        foreach ($except as $name) {
            unset($values[$name]);
        }
        return $values;
    }
}