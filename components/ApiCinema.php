<?php
namespace studiosite\cinemaccapi\components;

use Exception;
use GuzzleHttp\Client;
use InvalidArgumentException;
use studiosite\cinemaccapi\helpers\CinemaConverter;
use studiosite\cinemaccapi\helpers\Validator;
use studiosite\cinemaccapi\models\Movie;
use studiosite\cinemaccapi\models\Person;

/**
 * Компонент апи абстрактный для выгрузки
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class ApiCinema
{
    /**
     * @var string Ключ апи
     */
    public $apiKey = '';

    /**
     * @var string Формат запроса по умолчанию
     */
    public $defaultTypeRequest = 'GET';

    /**
     * @var string Формат ответа (xml|json)
     * @todo Поддержка других форматов, кроме json
     */
    public $format = 'json';

    /**
     * @var string Адрес сервера
     */
    public $host = 'http://api.cinemate.cc';

    /**
     * @var object instanceof Browser
     */
    private $_browser;

    /**
     * Конструктор компонента
     * @param string $host
     */
    public function __construct($properties)
    {
        foreach ($properties as $name => $value) {
            $this->$name = $value;
        }

        $this->init();
    }

    /**
     * Получить фильм по id
     * @param integer $id
     * @return \studiosite\cinemaccapi\models\Movie
     */
    public function getMovieById($id)
    {
        $response = $this->query('movie', [
            'id' => $id,
        ]);

        if (!$response || !isset($response['movie'])) {
            return false;
        }

        return CinemaConverter::getMovie($response['movie']);
    }

    /**
     * Поиск фильмов
     * @param string $term
     * @return \studiosite\cinemaccapi\models\Movie[]
     */
    public function getMoviesByTerm($term)
    {
        $response = $this->query('movie.search', [
            'term' => $term,
        ]);

        if (!$response || !isset($response['movie'])) {
            return [];
        }

        $result = [];

        if (empty($response['movie']['id'])) {
            foreach ($response['movie'] as $key => $value) {
                $result[] = CinemaConverter::getMovie($value);
            }
        } else {
            $result[] = CinemaConverter::getMovie($response['movie']);
        }

        return $result;
    }

    /**
     * Получить персону по id
     * @param integer $id
     * @return \studiosite\cinemaccapi\models\Person
     */
    public function getPersonById($id)
    {
        $response = $this->query('person', [
            'id' => $id,
        ]);

        if (!$response || !isset($response['person'])) {
            return false;
        }

        return CinemaConverter::getPersone($response['person']);
    }

    /**
     * Поиск персон
     * @param string $term
     * @return \studiosite\cinemaccapi\models\Movie[]
     */
    public function getPersonsByTerm($term)
    {
        $response = $this->query('person.search', [
            'term' => $term,
        ]);

        if (!$response || !isset($response['person'])) {
            return [];
        }

        return CinemaConverter::getPersons($response['person']);
    }

    /**
     * Инициализация компонента
     * Инициализация браузера
     */
    public function init()
    {
        $this->_browser = new Client();

        if (!Validator::host($this->host)) {
            throw new InvalidArgumentException('Invalid specified host');
        }
    }

    /**
     * Публичная функция запроса
     * @param string Пространство для запроса
     * @param array Параметры
     * @param string Тип запроса (GET|POST|...)
     */
    public function query(
        $requestNameSpace,
        $params,
        $typeRequest = false
    ) {
        $params = $this->setParams($params);

        return $this->_query($requestNameSpace, $params, $typeRequest);
    }

    /**
     * Получить обект парсера ответа
     *
     * @return \studiosite\cinemaccapi\interfaces\Parser
     */
    public function resolveTypeResponse($header)
    {
        $matches = [];

        if (preg_match('/[a-zA-Z0-9\\-]+\\/([a-zA-Z0-9\\-]+)/i', mb_strtolower(implode(";", $header)), $matches)) {
            $format = $matches[1];

            if ($format) {
                $parseClassName = '\\studiosite\\cinemaccapi\\parsers\\'.ucfirst($format);

                if (!class_exists($parseClassName)) {
                    throw new InvalidArgumentException('No support for the chosen format "'.$format.'"');
                }

                return (new $parseClassName());
            }
        }

        return false;
    }

    /**
     * Установка параметров для авторизации
     * @param array Параметры
     */
    public function setParams($params = [])
    {
        return array_merge($params, [
            'apikey' => $this->apiKey,
            'format' => $this->format,
        ]);
    }

    /**
     * Общий варинт запроса
     * @param string Пространство для запроса
     * @param array Параметры
     * @param string Тип запроса (GET|POST|...)
     */
    private function _query(
        $requestNameSpace,
        $params,
        $typeRequest = false
    ) {
        $typeRequest = $typeRequest ?: $this->defaultTypeRequest;
        $url = $this->host."/".$requestNameSpace;

        $response = $this->_browser->request($typeRequest, $url, [
            'query' => $params,
        ]);

        $responseCode = $response->getStatusCode();

        if ($responseCode !== 200) {
            throw new Exception('Invalid response code - '.$responseCode);
        }

        $responseFormatParse = $this->resolveTypeResponse($response->getHeader('content-type'));

        if ($responseFormatParse) {
            return $responseFormatParse->parse($response->getBody());
        }

        return $response->getBody();
    }
}
