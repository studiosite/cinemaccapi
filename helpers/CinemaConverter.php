<?php

namespace studiosite\cinemaccapi\helpers;

use studiosite\cinemaccapi\models\Movie;
use studiosite\cinemaccapi\models\Image;
use studiosite\cinemaccapi\models\Rating;
use studiosite\cinemaccapi\models\Person;
use studiosite\cinemaccapi\models\ListItem;

/**
 * Конвертер в модели
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 */
class CinemaConverter
{
	/**
	* Получить модель Movie из ответа апи
	* @param array Параметры
	* @return \studiosite\cinemaccapi\models\Movie
	*/
	public static function getMovie($params)
	{
		$movie = new Movie();

		// Установка постера
		if (!empty($params['poster'])) {
			$poster = new Image();
			$poster->setAttributes(array_map(function($item) {
				return \parse_url(@$item['url']) ? $item['url'] : false;
			}, $params['poster']));

			$movie->setAttributes(['poster' => $poster]);
			unset($params['poster']);
		}

		// Установка рейтинга imdb
		if (!empty($params['imdb'])) {
			$rating = new Rating();
			$rating->setAttributes([
				'value' => @$params['imdb']['rating'],
				'votes' => @$params['imdb']['votes'],
			]);

			$movie->setAttributes(['imdb' => $rating]);
			unset($params['imdb']);
		}

		// Установка рейтинга kinopoisk
		if (!empty($params['kinopoisk'])) {
			$rating = new Rating();
			$rating->setAttributes([
				'value' => @$params['kinopoisk']['rating'],
				'votes' => @$params['kinopoisk']['votes'],
			]);

			$movie->setAttributes(['kinopoisk' => $rating]);
			unset($params['kinopoisk']);
		}

		// Установка стран
		if (!empty($params['country']['name'])) {
			$items = self::getListItems($params['country']);
			$movie->setAttributes(['countries' => $items]);
			unset($params['country']);
		}

		// Установка жанров
		if (!empty($params['genre']['name'])) {
			$items = self::getListItems($params['genre']);
			$movie->setAttributes(['genres' => $items]);
			unset($params['genre']);
		}

		// Установка режисеров
		if (!empty($params['director']['person'])) {
			$items = self::getPersons($params['director']);
			$movie->setAttributes(['directors' => $items]);
			unset($params['director']);
		}

		// Установка в ролях
		if (!empty($params['cast']['person'])) {
			$items = self::getPersons($params['cast']['person']);
			$movie->setAttributes(['cast' => $items]);
			unset($params['cast']);
		}

		$movie->setAttributes($params);
		return $movie;
	}

	/**
	* Получить массив ListItem[] из ответа
	* @param array $params
	* @return \studiosite\cinemaccapi\models\ListItem[]
	*/
	public static function getListItems($params = [])
	{
		if (empty($params))
			return [];

		$items = [];
		if (is_array($params['name'])) {
			foreach ($params['name'] as $key => $value) {
				if (empty($value))
					continue;

				$item = new ListItem();
				$item->setAttributes(['name' => $value]);
				$items[] = $item;
			}
		} else {
			$item = new ListItem();
			$item->setAttributes($params);
			$items[] = $item;
		}

		return $items;
	}

	/**
	* Получить массив Person[] из ответа
	* @param array $params
	* @return \studiosite\cinemaccapi\models\Person[]
	*/
	public static function getPersons($params = [])
	{
		if (empty($params))
			return [];

		$items = [];
		if (empty($params['name'])) {
			foreach ($params as $key => $value) {
				$items[] = self::getPersone($value);
			}
		} else {
			$items[] = self::getPersone($params);
		}

		return $items;
	}

	/**
	* Получить Person из ответа
	* @param array $params
	* @return \studiosite\cinemaccapi\models\Person
	*/
	public static function getPersone($params = [])
	{
		if (empty($params))
			return;

		$item = new Person();
		if (!empty($params['photo'])) {
			$photo = new Image();
			$photo->setAttributes(array_map(function($itemArray) {
				return \parse_url(@$itemArray['url']) ? $itemArray['url'] : false;
			}, $params['photo']));

			$params['photo'] = $photo;
		}

		$item->setAttributes($params);
		return $item;
	}
}