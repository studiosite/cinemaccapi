<?php

namespace studiosite\cinemaccapi\models;

use studiosite\cinemaccapi\components\Model;

/**
 * Модель изображения.
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 * @property string $small
 * @property string $medium
 * @property string $big
 */
class Image extends Model
{
	/**
	* @var string Маленький размер (31x45)
	*/
	public $small;
	/**
	* @var string Средний размер (135x195)
	*/
	public $medium;
	/**
	* @var string Большой размер (260x385)
	*/
	public $big;
}