<?php
namespace studiosite\cinemaccapi\models;

use studiosite\cinemaccapi\components\Model;

/**
 * Модель фильма.
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 * @property integer $id
 * @property string $type
 * @property string $title_russian
 * @property string $title_original
 * @property string $title_english
 * @property integer $year
 * @property integer $year_finish
 * @property \parse_url() $url
 * @property string $description
 * @property \parse_url() $trailer
 * @property date $release_date_world
 * @property date $release_date_russia
 *
 * @property \studiosite\cinemaccapi\models\Image $poster
 * @property \studiosite\cinemaccapi\models\Rating $imdb
 * @property \studiosite\cinemaccapi\models\Rating $kinopoisk
 * @property \studiosite\cinemaccapi\models\ListItem[] $countries
 * @property \studiosite\cinemaccapi\models\ListItem[] $genres
 * @property \studiosite\cinemaccapi\models\Person[] $directors
 * @property \studiosite\cinemaccapi\models\Person[] $cast
 */
class Movie extends Model
{
	/**
	* @const Тип фильм
	*/
    const TYPE_MOVIE = 'movie';

    /**
	* @const Тип сериал
	*/
    const TYPE_SERIAL = 'serial';

    /**
     * @var \studiosite\cinemaccapi\models\Persone[] В ролях
     */
    public $cast;

    /**
     * @var \studiosite\cinemaccapi\models\ListItem[] Страны
     */
    public $countries;

    /**
     * @var string Описание
     */
    public $description;

    /**
     * @var \studiosite\cinemaccapi\models\Persone[] Режисеры
     */
    public $directors;

    /**
     * @var \studiosite\cinemaccapi\models\ListItem[] Жанры
     */
    public $genres;

    /**
     * @var integer #
     */
    public $id;

    /**
     * @var \studiosite\cinemaccapi\models\Rating Рейтинг imdb
     */
    public $imdb;

    /**
     * @var \studiosite\cinemaccapi\models\Rating Рейтинг кинопоиск
     */
    public $kinopoisk;

    /**
     * @var \studiosite\cinemaccapi\models\Image Постер в разных размерах
     */
    public $poster;

    /**
     * @var date Релиз в россии (Y-m-d)
     */
    public $release_date_russia;

    /**
     * @var date Релиз в мире (Y-m-d)
     */
    public $release_date_world;

    /**
     * @var integer Продолжительность в минутах
     */
    public $runtime;

    /**
     * @var string Название на английском
     */
    public $title_english;

    /**
     * @var string Название оригинальное
     */
    public $title_original;

    /**
     * @var string Название на русском
     */
    public $title_russian;

    /**
     * @var \parse_url() Трейлер
     */
    public $trailer;

    /**
     * @var string Тип
     */
    public $type;

    /**
     * @var \parse_url() Адрес описание на сайте
     */
    public $url;

    /**
     * @var integer Год начала
     */
    public $year;

    /**
     * @var integer Год окончания
     */
    public $year_finish;

    /**
     * Возрасные ограничения
     *
     * @return \studiosite\cinemaccapi\models\Persone[]
     */
    public function getActors()
    {
        return $this->cast;
    }

    /**
     * Возрасные ограничения
     *
     * @return integer
     */
    public function getAgeRestricted()
    {
        return 0;
    }

    /**
     * Актерский состав строкой
     *
     * @return string
     */
    public function getCast()
    {
        if (empty($this->cast)) {
            return '';
        }

        return implode(", ", array_map(function ($item) {
            return $item->name;
        }, $this->cast));
    }

    /**
     * Страны
     *
     * @return \studiosite\cinemaccapi\models\ListItem
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Страны строко
     *
     * @return string
     */
    public function getCountriesString()
    {
        if (empty($this->countries)) {
            return '';
        }

        return implode(", ", array_map(function ($item) {
            return $item->name;
        }, $this->countries));
    }

    /**
     * Описание
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Режисеры
     *
     * @return \studiosite\cinemaccapi\models\ListItem
     */
    public function getDirectors()
    {
        return $this->directors;
    }

    /**
     * Режисеры строкой
     *
     * @return string
     */
    public function getDirectorsString()
    {
        if (empty($this->directors)) {
            return '';
        }

        return implode(", ", array_map(function ($item) {
            return $item->name;
        }, $this->directors));
    }

    /**
     * Жанры
     *
     * @return \studiosite\cinemaccapi\models\ListItem
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * Жанры строкой
     *
     * @return string
     */
    public function getGenresString()
    {
        if (empty($this->genres)) {
            return '';
        }

        return implode(", ", array_map(function ($item) {
            return $item->name;
        }, $this->genres));
    }

    /**
     * Рейтинг IMDb
     *
     * @return \studiosite\cinemaccapi\models\Rating
     */
    public function getIMDbRating()
    {
        return $this->imdb ?: (new \studiosite\cinemaccapi\models\Rating());
    }

    /**
     * Сериал или нет
     *
     * @return boolean
     */
    public function getIsSerial()
    {
        return $this->type === self::TYPE_SERIAL;
    }

    /**
     * Рейтинг кинопоиск
     *
     * @return \studiosite\cinemaccapi\models\Rating
     */
    public function getKinopoiskRating()
    {
        return $this->kinopoisk ?: (new \studiosite\cinemaccapi\models\Rating());
    }

    /**
     * Объект постера
     *
     * @return \studiosite\cinemaccapi\models\Image
     */
    public function getPoster()
    {
        return $this->poster ?: (new \studiosite\cinemaccapi\models\Image());
    }

    /**
     * Слоган
     *
     * @return string
     */
    public function getSlogan()
    {
        return '';
    }

    /**
     * Название
     *
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title_russian;
    }

    /**
     * Оригинальное название
     *
     * @return string
     */
    public function getTitleOriginal()
    {
        return $this->title_original ?: $this->title_russian;
    }

    /**
     * Тип (self::TYPE_MOVIE|self::TYPE_SERIAL)
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type ?: self::TYPE_MOVIE;
    }

    /**
     * Получить год окончания выпуска
     *
     * @return mixed
     */
    public function getYearEnd()
    {
        return $this->year_finish ?: $this->year;
    }

    /**
     * Год начала выпоска
     *
     * @return string
     */
    public function getYearStart()
    {
        return $this->year;
    }

    /**
     * Года ("2000-2016")
     *
     * @return string
     */
    public function getYears()
    {
        if (empty($this->year_finish)) {
            return $this->year;
        }

        return $this->year."-".$this->year_finish;
    }
}
