<?php
namespace studiosite\cinemaccapi\models;

use studiosite\cinemaccapi\components\Model;

/**
 * Модель итема списка.
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 * @property string $name
 */
class ListItem extends Model
{
    /**
     * @var string Имя
     */
    public $name;
}
