<?php
namespace studiosite\cinemaccapi\models;

use studiosite\cinemaccapi\components\Model;

/**
 * Модель персоны.
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 * @property integer $id
 * @property string $name
 * @property string $name_original
 * @property \parse_url() $url
 *
 * @property \studiosite\cinemaccapi\models\Image $photo
 * @property \studiosite\cinemaccapi\models\Movie $photo
 */
class Person extends Model
{
    /**
     * @var integer #
     */
    public $id;

    /**
     * @var \studiosite\cinemaccapi\models\Movie Фотография
     */
    public $movies;

    /**
     * @var string Имя
     */
    public $name;

    /**
     * @var string Имя на оригинальном языке
     */
    public $name_original;

    /**
     * @var \studiosite\cinemaccapi\models\Image Фотография
     */
    public $photo;

    /**
     * @var \parse_url() Адрес
     */
    public $url;
}
