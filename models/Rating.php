<?php
namespace studiosite\cinemaccapi\models;

use studiosite\cinemaccapi\components\Model;

/**
 * Модель рейтинга.
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 * @property float $value
 * @property integer $votes
 */
class Rating extends Model
{
    /**
     * @var float Рейтинг
     */
    public $value;

    /**
     * @var integer Количество голосов
     */
    public $votes;
}
